<table>
    <thead>
        <tr>
        @foreach($header as $h)
            <th>{{$h}}</th>
        @endforeach
        </tr>
    </thead>

    <tbody>
        @foreach($data as $d)
        <tr>
        <?php
            for($i = 0; $i < count($d); $i++) {
                echo "<td>".$d[$i]."</td>";
            }
        ?>
        </tr>
        @endforeach
    </tbody>
</table>

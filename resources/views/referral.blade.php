@forelse(auth()->user()->getReferrals() as $referral)
    <h4>
        Program: {{ $referral->program->name }}
    </h4>
    <code>
        Referral link: {{ $referral->link }}
    </code>
    <p>
        Number use referral link: {{ $referral->relationships()->count() }}
    </p>
@empty
    No referral program!
@endforelse

<?php

use Illuminate\Database\Seeder;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 10000; $i++) {
            DB::table('test_model1s')->insert([
                'name' => 'model 1_'.$i,
                'model2_id' => rand(10007, 30000)
            ]);
        }
        for($i = 1; $i < 10000; $i++) {
            DB::table('test_model2s')->insert([
                'name' => 'model 2_'.$i,
                'model1_id' => rand(1, 10007)
            ]);
        }
    }
}

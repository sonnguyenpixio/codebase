<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestModel1 extends Model
{
    public function model2() {
        return $this->belongsTo('App\TestModel2');
    }
}

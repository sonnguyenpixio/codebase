<?php

namespace App\Http\Controllers;

use Mail;

use Illuminate\Http\Request;
use App\Mail\MailTemplate;

class MailController extends Controller
{
    public function send() {
        $fromEmail = "hello@pixiostudio.com";
        $subject = "Testing Email";
        $fileName = "data/example.txt";
        Mail::to("sonnt@pixiostudio.com")->send(new MailTemplate($fromEmail, $subject, $fileName));
    }
}

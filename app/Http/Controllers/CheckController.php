<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Word;
use App\Label;
use App\Sentence;
use App\Helpers\LabelForSentence;

class CheckController extends Controller
{
    public function __construct(){
        $this->words = Word::where([])->orderBy('new_word', 'asc')->get();
    }

    private function isNumber($ch) {
        $chars = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        foreach($chars as $char) {
            if ($ch == $char) return true;
        }
        return false;
    }

    private function isDot($ch) {
        $chars = [".", ",", "!", ":"];
        foreach($chars as $char) {
            if ($ch == $char) return true;
        }
        return false;
    }

    private function isCharacter($ch) {
        return (!$this->isNumber($ch) && !$this->isDot($ch));
    }

    private function addSpace($str) {
        // add space to sentence
        $word1s = ["m","m"];
        $word2s = ["d","n"];
        for($i = 0; $i < count($word1s); $i++) {
            $str = str_replace($word1s[$i].$word2s[$i], $word1s[$i]." ".$word2s[$i], $str);
        }
        $result = "";
        for($i = 0; $i < strlen($str) - 1; $i++) {
            if (($this->isNumber($str[$i]) && $this->isCharacter($str[$i + 1])
            && $str[$i + 1] !== "k" && $str[$i + 1] !== "%")
            ||  $this->isCharacter($str[$i]) && $this->isNumber($str[$i + 1])) {
                $result = $result.$str[$i]." ";
            } else {
                $result = $result.$str[$i];
            }
        }
        $result = $result.$str[strlen($str) - 1];
        return $result;
    }

    private function specialCase($str) {
        for($i = 0; $i < 10; $i++) {
            $str = str_replace($i." hop ", $i." hộp ", $str);
            $str = str_replace($i." họp ", $i." hộp ", $str);
            $str = str_replace($i." hôp ", $i." hộp ", $str);
            $str = str_replace($i." ho ", $i." hộp ", $str);
            $str = str_replace($i." hốp ", $i." hộp ", $str);
            $str = str_replace($i." hợp ", $i." hộp ", $str);
            $str = str_replace($i." gọp ", $i." hộp ", $str);
            $str = str_replace($i." gop ", $i." hộp ", $str);
            $str = str_replace($i." gộp ", $i." hộp ", $str);
            $str = str_replace($i." hu ", $i." hủ ", $str);
            $str = str_replace($i." hú ", $i." hủ ", $str);
            $str = str_replace($i." hũ ", $i." hủ ", $str);
            $str = str_replace($i." hụ ", $i." hủ ", $str);
            $str = str_replace($i." hủz ", $i." hủ ", $str);
            $str = str_replace($i." hux ", $i." hủ ", $str);
            $str = str_replace($i." hư ", $i." hủ ", $str);
            $str = str_replace($i." hủr ", $i." hủ ", $str);
            $str = str_replace($i." hủu ", $i." hủ ", $str);
            $str = str_replace($i." hủ̃u ", $i." hủ ", $str);
            $str = str_replace($i." hữu ", $i." hủ ", $str);
            $str = str_replace($i." bo ", $i." bộ ", $str);
            $str = str_replace($i." bọ ", $i." bộ ", $str);
            $str = str_replace($i." bô ", $i." bộ ", $str);
            $str = str_replace($i." bộn ", $i." bộ ", $str);
            $str = str_replace($i." lo ", $i." lọ ", $str);
            $str = str_replace($i." lọj ", $i." lọ ", $str);
            $str = str_replace($i." lan ", $i." lần ", $str);
            $str = str_replace($i." no ", $i." lọ ", $str);
            $str = str_replace($i." nọ ", $i." lọ ", $str);
            $str = str_replace($i." cai ", $i." cái ", $str);
            $str = str_replace($i." cai ", $i." cái ", $str);
            $str = str_replace($i." chaj ", $i." chai ", $str);
            $str = str_replace($i." chay ", $i." chai ", $str);
            $str = str_replace($i." trai ", $i." chai ", $str);
        }
        $units = ["hủ", "hộp", "chai", "combo", "bộ",  "cái", "gói"];
        for($i = 0; $i < 10; $i++) {
            foreach($units as $unit) {
                $str = str_replace($i." ".$unit." bn", $i." ".$unit." bao nhiêu", $str);
            }
        }
        for($i = 0; $i < 10; $i++) {
            $str = str_replace($i." th ", $i." tháng ", $str);
            $str = str_replace($i." tuan ", $i." tuần ", $str);
        }
        $str = str_replace(" .", ".", $str);
        return $str;
    }

    private function remove2Space($str) {
        do {
             $str = str_replace("  ", " ", $str);
        } while (strpos($str, "  "));
        return $str;
    }

    private function checkSpell($str) {
        $str = $this->remove2Space($str);
        $str = mb_strtolower($str);
        $str = " ".$str." ";
        $str = $this->addSpace($str);
        $str = $this->remove2Space($str);
        foreach($this->words as $word) {
            if (strpos($str, $word->original_word) !== FALSE) {
                // echo $word->original_word."\n";
                // echo "BEFORE: ".$str."\n";
                $str = str_replace($word->original_word, $word->new_word, $str);
                $str = $this->remove2Space($str);
                // echo "AFTER: ".$str."\n";
                // echo $word->new_word."\n";
            }
        }
        $str = $this->remove2Space($str);
        $str = $this->specialCase($str);
        if ($str[0]) $str = substr($str, 1, strlen($str) - 1);
        return $str;
    }

    private function removeSepcialCharacter($str) {
        $str = preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "\n", $str));
        $str = str_replace("\n", "", $str);
        $str = str_replace("\r", "", $str);
        $str = str_replace("⁰", "", $str);
        return $str;
    }

    public function message(Request $request)
    {
        $sentence = $request->input('message');
        $str = " ".$this->removeSepcialCharacter($sentence)." ";
        $oldStr = $str;
        do {
            $oldStr = $str;
            $str = str_replace("..",".",$oldStr);
        } while ($oldStr != $str);
        do {
            $oldStr = $str;
            $str = $this->checkSpell($oldStr);
        } while ($oldStr != $str);
        // return $str;
        $label = LabelForSentence::setLabel($str);
        return json_encode($label);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class RouteController extends Controller
{
    public function index() {
        // signature and expire links
        return URL::temporarySignedRoute(
            'unsubscribe', now()->addMinutes(1), ['user' => 1]
        );
        // just signature
        return URL::signedRoute('unsubscribe', ['userId' => 1]);
    }

    public function test(Request $request) {
        if ($request->hasValidSignature()) {
            echo "true";
        } else {
            abort(401);
        }
    }
}

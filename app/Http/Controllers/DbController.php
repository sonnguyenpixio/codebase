<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\TestModel1;
use App\TestModel2;
use App\Helpers\Pagination;

class DbController extends Controller
{
    public function eloquent() {
        $model1 = TestModel1::first();
        var_dump($model1->model2->name);
    }

    public function join() {
        $data = DB::table('test_model1s')
                ->join('test_model2s', 'test_model2s.id', '=', 'test_model1s.model2_id')
                ->select([
                    'test_model2s.name as model2_name',
                    'test_model1s.name as model1_name'
                ])->get();
        $listPages = Pagination::initArray(1, 4);
        var_dump($data);
    }

    public function chunk() {
        TestModel1::where('is_processed', 0)->chunkById(100, function($modelArr) {
            foreach($modelArr as $modelObj) {
                $modelObj->is_processed = 1;
                $modelObj->save();
            }
        });
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Event;

class ViewController extends Controller
{
    public function count() {
        Event::dispatch('posts.view', '/homepage');
        return view('welcome');
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiUserController extends Controller
{
    public function get() {
        return "get";
    }
    public function post() {
        return "post";
    }
}

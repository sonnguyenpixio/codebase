<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\PusherEvent;

class PusherController extends Controller
{
    public function getPusher(){
        return view("demo-pusher");
    }

    public function fireEvent(){
        event(new PusherEvent("Hi, I'm Son!"));
        return "Message has been sent.";
    }
}

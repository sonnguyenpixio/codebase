<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Excel;
use App\Exports\ExampleTemplate;

class FileController extends Controller
{
    public function saveDataToExcel() {
        $fileName = "exmple_excel.xlsx";
        $header = ["value 1", "value 2", "value 3"];
        $data = [
            ["a", "b", "c"],
            ["d", "e", "f"]
        ];
        $templateExcel = new ExampleTemplate($data, $header);
        Excel::store($templateExcel, $fileName);
    }

    public function readCsvFile() {
        $count = 0;
        $file = fopen("data/data.csv","r");
        while(!feof($file)) {
            $data = fgetcsv($file);
            if (is_array($data) && count($data) > 3 && $count) {
                $images = explode(",",$data[2]);
                $title = $data[0];
                $category = $data[3];
            }
            $count++;
        }
        fclose($file);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class EnvironmentController extends Controller
{
    public function get() {
        $environment = App::environment();
        $value = config('app.timezone');
        echo $value;
    }
}

<?php

namespace App\Events;

use App\PostView;
use Illuminate\Session\Store;
use Illuminate\Support\Carbon;

class ViewPostHandler {
    public function __construct(Store $session) {
        $this->session = $session;
    }

    public function handle($url) {
        if (!$this->isPostViewed($url)) {
            $post = PostView::where('url', $url)->first();
            if (!$post) {
                $post = new PostView;
                $post->url = $url;
                $post->save();
            } else {
                $to = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:s:i');
                $today = Carbon::createFromFormat('Y-m-d H:s:i', $to);
                $from = Carbon::createFromFormat('Y-m-d H:s:i', $post->updated_at);
                $diff_in_days = $today->diffInDays($from);
                if ($diff_in_days > 0) {
                    $post->view_count = 1;
                    $post->save();
                } else {
                    $post->increment('view_count');
                }
            }
            $this->storePost($url);
        }
    }

    private function isPostViewed($url) {
        $viewed = $this->session->get('viewed_posts', []);
        return array_key_exists($url, $viewed);
    }

    private function storePost($url) {
        $key = 'viewed_posts.' . $url;
        $this->session->put($key, time());
    }
}

?>

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Storage;

class MailTemplate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $_from;
    public $_subject;
    public $_fileName;

    public function __construct($from,$subject,$fileName) {
        $this->_from = $from;
        $this->_subject = $subject;
        $this->_fileName = $fileName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $eFrom = $this->_from;
        $eSubject = $this->_subject;
        $eFileName = $this->_fileName;
        $linkAttachment = $eFileName;
        return $this->from($eFrom)->subject($eSubject)
                    ->view('mail-template.template1',[
                        'name' => 'Pixio Studio'
                    ])->attach($linkAttachment, [
                        'as' => $eFileName,
                        'mime' => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    ]);
    }
}

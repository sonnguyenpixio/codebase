<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExampleTemplate implements FromView
{

	public $data;
	public $header;

	public function __construct($data="",$header="")
	{
	    $this->data = $data;
	    $this->header = $header;
	}

    public function view(): View
    {
        return view('export-template.template1', [
            'data' => $this->data,
            'header' => $this->header,
        ]);
    }

}

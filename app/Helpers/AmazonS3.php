<?php

namespace App\Helpers;

use Storage;

class AmazonS3
{
    public static function upload($file, $folder) {
        $url = 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';
        Storage::disk('s3')->put($folder . '/' . $file->getClientOriginalName(), file_get_contents($file));
        return $url . $folder . '/' . $file->getClientOriginalName();
    }

    public static function uploadRandom($file, $folder) {
        $url = 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';
        $fileName = substr(md5($file->getClientOriginalName() . date("Y-m-d h:i:sa")), 15) . '.' . $file->getClientOriginalExtension();
        Storage::disk('s3')->put($folder . '/' . $fileName, file_get_contents($file));
        return $url . $folder . '/' . $fileName;
    }

    public static function delete($filename) {
        $url = 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';
        $name = str_replace($url, '', $filename);
        Storage::disk('s3')->delete($name);
    }

    public static function getAllFile() {
        $files = Storage::disk('s3')->files();
        return $files;
    }
}

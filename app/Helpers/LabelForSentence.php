<?php

namespace App\Helpers;

use App\Label;
use App\Helpers\LabelForSentence;

class LabelForSentence
{
    public static function isQuestion($sentence) {
        if (strpos($sentence, "hả em") !== FALSE) return true;
        if (strpos($sentence, "?") !== FALSE) return true;
        if (strpos($sentence, "đúng không") !== FALSE) return true;
        return false;
    }

    public static function isBuy($sentence) {
        $units = ["hủ", "hộp", "chai", "combo", "bộ", "cái", "gói", "short", "liệu trình",
        "lọ", "đôi", "hũ", "gel", "sản phẩm", "áo", "quần", "tuýp", "tuyp"];
        $subjects = ["chị", "em", "mình", "anh", "tớ", "tôi", "minh"];
        $buyVerbs = ["ship", "lấy", "mua", "nhận", "lấy thêm", "mua thêm", "lấy cả", "lay", "gửi", "đặt", "thêm", "cho", "gởi"];
        $timePhases = ["mai", "mốt", "tuần sau", "tuần tới", "tuần sau", "tuần này",
        "thứ hai", "thứ ba", "thứ tư", "thứ năm", "thứ sáu", "thứ bảy", "chủ nhật"];
        $numbers = ["một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười"];
        $otherWords = ["hộ", "ạ", "nhá", "nhé", "đi", "cho", "thôi"];
        $products = ["đắp mặt", "mặt rồng phượng", "vòng tay", "sản phẩm"];
        $promotions = ["2 tặng 1"];
        // lấy hủ
        foreach ($buyVerbs as $verb) {
            foreach($units as $unit) {
                $phase = $verb." ".$unit;
                if (strpos($sentence, $phase) !== FALSE) return true;
            }
        }
        // lấy cái đắp mặt
        foreach ($buyVerbs as $verb) {
            foreach($units as $unit) {
                foreach($products as $product) {
                    $phase = $verb." ".$unit." ".$product;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy cho chị
        foreach ($buyVerbs as $verb) {
            foreach($otherWords as $word) {
                foreach($subjects as $subject) {
                    $phase = $verb." ".$word." ".$subject;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // mình 1 lọ
        foreach($subjects as $subject) {
            for($i = 1; $i < 100; $i++) {
                foreach($units as $unit) {
                    $phase = $subject." ".$i." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // mình một lọ
        foreach($subjects as $subject) {
            foreach($numbers as $number) {
                foreach($units as $unit) {
                    $phase = $subject." ".$number." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy 1 ạ
        foreach ($buyVerbs as $verb) {
            for($i = 1; $i < 100; $i++) {
                foreach($otherWords as $word) {
                    $phase = $verb." ".$i." ".$word;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy một ạ
        foreach ($buyVerbs as $verb) {
            foreach($numbers as $number) {
                foreach($otherWords as $word) {
                    $phase = $verb." ".$number." ".$word;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy chị một
        foreach ($buyVerbs as $verb) {
            foreach($subjects as $subject) {
                foreach($numbers as $number) {
                    $phase = $verb." ".$subject." ".$number;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy một hủ
        foreach ($buyVerbs as $verb) {
            foreach($numbers as $number) {
                foreach($units as $unit) {
                    $phase = $verb." ".$subject." ".$number." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy 1 hủ
        foreach ($buyVerbs as $verb) {
            for($i = 1; $i < 100; $i++) {
                foreach($units as $unit) {
                    $phase = $verb." ".$i." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy chị 1 hủ
        foreach ($buyVerbs as $verb) {
            foreach($subjects as $subject) {
                for($i = 0; $i < 100; $i++) {
                    foreach($units as $unit) {
                        $phase = $verb." ".$subject." ".$i." ".$unit;
                        if (strpos($sentence, $phase) !== FALSE) return true;
                    }
                }
            }
        }
        // lấy 1 hủ
        foreach ($buyVerbs as $verb) {
            for($i = 0; $i < 100; $i++) {
                foreach($units as $unit) {
                    $phase = $verb." ".$subject." ".$i." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // chị 2 hủ
        foreach($subjects as $subject) {
            for($i = 0; $i < 100; $i++) {
                foreach($units as $unit) {
                    $phase = $subject." ".$i." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // chị hai hủ
        foreach($subjects as $subject) {
            foreach($numbers as $number) {
                foreach($units as $unit) {
                    $phase = $subject." ".$number." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy một hủ nhé
        foreach ($buyVerbs as $verb) {
            foreach($numbers as $number) {
                foreach($units as $unit) {
                    $phase = $verb." ".$number." ".$unit;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // mai em lấy
        foreach ($timePhases as $timePhase) {
            foreach($subjects as $subject) {
                foreach($buyVerbs as $verb) {
                    $phase = $timePhase." ".$subject." ".$verb;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // 1 hộp thôi
        for($i = 0; $i < 100; $i++) {
            foreach($units as $unit) {
                foreach($otherWords as $word) {
                    $phase = $i." ".$unit." ".$word;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // một hộp thôi
        foreach($numbers as $number) {
            foreach($units as $unit) {
                foreach($otherWords as $word) {
                    $phase = $number." ".$unit." ".$word;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        // lấy combo 2 tặng 1
        foreach ($buyVerbs as $verb) {
            foreach($units as $unit) {
                foreach($promotions as $promotion) {
                    $phase = $verb." ".$unit." ".$promotion;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        return false;
    }

    public static function isNumber($ch) {
        $chars = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        foreach($chars as $char) {
            if ($ch == $char) return true;
        }
        return false;
    }

    public static function isContainPhoneNumber($str) {
        $oldStr = $str;
        do {
            $oldStr = $str;
            $str = str_replace(". ","",$oldStr);
        } while ($oldStr != $str);
        for($i = 0; $i < strlen($str) - 8; $i++) {
            $count = 0;
            $checkStr = substr($str, $i, 8);
            for($j = 0; $j < strlen($checkStr); $j++) {
                if (LabelForSentence::isNumber($checkStr[$j])) $count++;
            }
            if ($count > 5) return true;
        }
        return false;
    }

    public static function isContainAddress($sentence) {
        $label = Label::where('value', '3.2 - cung cap thong tin - dia chi')->first();
        $keywords = json_decode($label->keywords);
        if (count($keywords)) {
            foreach($keywords as $keyword) {
                if (strpos($sentence, $keyword) !== FALSE) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function isContainPrice($sentence) {
        // 512k
        $words = explode(" ",$sentence);
        foreach($words as $word) {
            $length = strlen($word);
            if ($length > 1 && $word[$length - 1] == "k") {
                $textSubstr = substr($word, 0, $length - 1);
                if (is_numeric($textSubstr)) return true;
            }
        }
        return false;
    }

    public static function isAskPrice($sentence) {
        if (LabelForSentence::isContainPrice($sentence)) return true;
        $units = ["hủ", "hộp", "chai", "combo", "bộ", "cái", "gói", "short", "liệu trình",
        "lọ", "đôi", "hũ", "gel", "sản phẩm", "áo", "quần", "tuýp", "tuyp"];
        $numbers = ["một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười"];
        $askWords = ["mấy","nhiêu","may"];
        foreach($numbers as $number) {
            foreach($units as $unit) {
                foreach($askWords as $word) {
                    $phase = $number." ".$unit." ".$word;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        for($i = 1; $i < 100; $i++) {
            foreach($units as $unit) {
                foreach($askWords as $word) {
                    $phase = $i." ".$unit." ".$word;
                    if (strpos($sentence, $phase) !== FALSE) return true;
                }
            }
        }
        return false;
    }

    public static function isContainPromotion($sentence) {
        $units = ["hủ", "hộp", "chai", "combo", "bộ", "cái", "gói", "short", "liệu trình",
        "lọ", "đôi", "hũ", "gel", "sản phẩm", "áo", "quần", "tuýp", "tuyp"];
        $numbers = ["một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười"];
        // 10%
        $words = explode(" ",$sentence);
        foreach($words as $word) {
            $length = strlen($word);
            if ($length > 1 && $word[$length - 1] == "%") {
                $textSubstr = substr($word, 0, $length - 1);
                if (is_numeric($textSubstr)) return true;
            }
        }
        // mua 3 tặng 1
        for($i = 1; $i < 10; $i++) {
            $phase = "mua ".$i." tặng";
            if (strpos($sentence, $phase) !== FALSE) return true;
            $phase = "mua ".$i." tang";
            if (strpos($sentence, $phase) !== FALSE) return true;
            for($j = 1; $j < 10; $j++) {
                $phase = $i." tặng ".$j;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = $i." tang ".$j;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = $i." tăng ".$j;
                if (strpos($sentence, $phase) !== FALSE) return true;
            }
        }
        // một tặng một
        foreach($numbers as $number1) {
            foreach($numbers as $number2) {
                $phase = $number1." tặng ".$number2;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = $number1." tang ".$number2;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = $number1." tăng ".$number2;
                if (strpos($sentence, $phase) !== FALSE) return true;
            }
        }
        // tặng 1 hũ
        for($i = 1; $i < 10; $i++) {
            foreach($units as $unit) {
                $phase = "tang ".$i." ".$unit;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = "tặng ".$i." ".$unit;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = "tăng ".$i." ".$unit;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = "tag ".$i." ".$unit;
                if (strpos($sentence, $phase) !== FALSE) return true;
                $phase = "tặg ".$i." ".$unit;
                if (strpos($sentence, $phase) !== FALSE) return true;
            }
        }
        return false;
    }

    public static function processLabel($labelResult, $sentence) {
        // check trường hợp bộ 3 nhãn 3.2 | 3.1 | 3.6
        if ($labelResult == "3.2 - cung cap thong tin - dia chi") {
            if (LabelForSentence::isContainPhoneNumber($sentence)) {
                $labelResult = "3.6 - cung cap thong tin - cung cap so dien thoai + dia chi";
            }
        }
        if ($labelResult == "3.1 - cung cap thong tin - so dien thoai") {
            if (LabelForSentence::isContainAddress($sentence)) {
                $labelResult = "3.6 - cung cap thong tin - cung cap so dien thoai + dia chi";
            }
        }
        // check trường hợp hỏi đơn vị chứ không phải giá
        if ($labelResult == "1.4 - hoi - gia") {
            $units = ["viên", "miếng", "cái"];
            foreach($units as $unit) {
                $word = "bao nhiêu ".$unit;
                if (strpos($sentence, $word) !== FALSE && substr_count($sentence, "bao nhiêu") < 2) {
                    $labelResult = "1.6 - hoi - khac";
                }
            }
        }
        return $labelResult;
    }

    public static function removeDuplicateArr($arr) {
        $res = [];
        for($i = 0; $i < count($arr); $i++) {
            $ok = true;
            for($j = $i - 1; $j >= 0; $j--) {
                if ($arr[$j] == $arr[$i]) {
                    $ok = false;
                    break;
                }
            }
            if ($ok) {
                array_push($res, $arr[$i]);
            }
        }
        return $res;
    }

    public static function setLabel($sentence) {
        $labels = Label::all();
        $labelResult = "";
        $results = [];
        // lấy ra các label có thể có
        foreach ($labels as $label) {
            $keywords = json_decode($label->keywords);
            if (count($keywords)) {
                foreach($keywords as $keyword) {
                    if (strpos($sentence, $keyword) !== FALSE) {
                        $labelResult = $label->value;
                        array_push($results, $labelResult);
                        break;
                    }
                }
            }
        }
        // xử lý trường hợp câu có ý hỏi nhưng chưa có trong list từ khoá
        // xử lý trường hợp bao gồm số điện thoại
        $isQuestion = false;
        $processResults = [];
        $labelResult = "";
        if (LabelForSentence::isQuestion($sentence)) {
            $labelResult = "1.1 - hoi - tu van";
            $isQuestion = true;
        } else {
            if (LabelForSentence::isContainPhoneNumber($sentence)) {
                $labelResult = "3.1 - cung cap thong tin - so dien thoai";
            }
        }
        if ($labelResult) {
            array_push($results, $labelResult);
        }
        // xử lý câu có bao gồm giá
        if (LabelForSentence::isAskPrice($sentence)) {
            array_push($results, "1.4 - hoi - gia");
        }
        // xủ lý bao gồm khuyến mãi
        if (LabelForSentence::isContainPromotion($sentence)) {
            array_push($results, "1.5 - hoi - khuyen mai");
        }
        // xử lý trường hợp câu có ý mua nhưng chưa có trong list từ khoá
        // nếu nhãn vừa lấy ra là undefined chỉ đẩy vào khi mảng label đang rỗng
        if (LabelForSentence::isBuy($sentence)) {
            $labelResult = "4 - mua hang";
            array_push($results, $labelResult);
        }
        // xử lý một số trường hợp các bộ nhãn liên quan với nhau
        if (count($results)) {
            foreach($results as $result) {
                array_push($processResults, LabelForSentence::processLabel($result, $sentence));
            }
        }
        if (!count($processResults)) {
            $labelResult = "0 - Undefined";
            array_push($processResults, $labelResult);
        }
        // loại bỏ các nhãn trùng
        $processResults = LabelForSentence::removeDuplicateArr($processResults);
        return $processResults;
    }
}

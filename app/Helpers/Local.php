<?php

namespace App\Helpers;

use File;

class Local
{
    public static function upload($file,$path)
    {
        $fileName = substr(md5($file->getClientOriginalName() . date("Y-m-d h:i:sa")), 15) . '.' . $file->getClientOriginalExtension();
        $file->move($path, $fileName);
        $fileName = $path.'/' . $fileName;
        return $fileName;
    }

    public static function delete($url){
        File::delete(base_path() . '/' . $url);
    }
}

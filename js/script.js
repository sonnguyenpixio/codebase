$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#chat__form').on("submit",function(e) {
    var message = $('.input__chat').val();
    e.preventDefault();
    $.ajax({
        type    :   'POST',
        url     :   '/chat/message',
        data    :   { message : message },
    })
    .done(function(result) {
        alert(result);
    });
});

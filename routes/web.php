<?php

Route::get('/', function(){
    return view('welcome');
});

Route::get('/mail/send', 'MailController@send');

Route::get('/db/eloquent', 'DbController@eloquent');
Route::get('/db/join', 'DbController@join');
Route::get('/db/chunk', 'DbController@chunk');

Route::get('/file/save-data-to-excel', 'FileController@saveDataToExcel');
Route::get('/file/read-csv-file', 'FileController@readCsvFile');

Route::get('/environment', 'EnvironmentController@get');

Route::get('/pusher/show','PusherController@getPusher');
Route::get('/pusher/fire-event','PusherController@fireEvent');

// limit request per minute
Route::middleware('throttle:2,1')->group(function () {
    Route::get('/referral', function(){
        return view('welcome');
    });
});

Route::middleware('auth', 'throttle:rate_limit,1')->group(function () {
    Route::get('/test-limit-route', function(){
        return view('welcome');
    });
});

Route::middleware('throttle:1|rate_limit,1')->group(function () {
    Route::get('/test-limit-route-guest-user', function(){
        return view('welcome');
    });
});

Route::middleware('throttle:1|rate_limit,1')->group(function () {
    Route::get('/test-limit-route-guest-user', function(){
        return view('welcome');
    });
});

Route::get('/test-middleware/{id}', function(){
    return view('welcome');
})->middleware('check.age');

Auth::routes();

Route::resource('photos', 'PhotoController');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/unsubscribe/{userId}', 'RouteController@test')->name('unsubscribe');

Route::post('/chat/message', 'CheckController@message');

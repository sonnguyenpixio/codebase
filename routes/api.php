<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('my-profile', 'Api\AuthController@user');
    });
});

Route::group([
    'prefix' => 'user'
], function () {
    Route::get('detail', 'Api\UserController@get');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('detail', 'Api\UserController@post');
    });
});
